



open HolKernel Parse Term
open symbExp
open symbStmt
open symbJumps
open symbProgram
open Redblackmap
open bir_programTheory bir_expTheory


structure unitTest = 
struct


(**** Some expressions *****)
val constExp1 = ``BExp_Const (Imm64 12w)``
val constExp2 = ``BExp_Const (Imm64 0x12w)``
val empty = Redblackmap.mkDict (String.compare)
fun test_const_exp () =
	let 
		val st1 = (translate_exp constExp1)  empty
		val st2 = (translate_exp constExp2) empty
		val _ = print st1
		val _ = print "\n"
		val _ = print st2
		val _ = print "\n"
	in
		()
	end
	

val denvarExp1 = ``BExp_Den (BVar "lol" (BType_Imm Bit64))``
val map = Redblackmap.fromList (String.compare) [("lol", "(_ bv23 64)")]	
fun test_denvar_exp () =
		print ((translate_exp denvarExp1) map)


val predExp1 = ``BExp_BinPred BIExp_Equal (^constExp1) (^constExp1)``
fun test_predExp () =
		print((translate_exp predExp1) map)


val binExp1 = ``BExp_BinExp BIExp_And (^denvarExp1) (^denvarExp1)``
fun test_binExp () = 
		print((translate_exp binExp1) map)
val unaryExp = ``BExp_UnaryExp BIExp_ChangeSign (^binExp1)``
fun test_unary() = print ((translate_exp unaryExp) map)
	
val castExp1 = ``BExp_Cast BIExp_UnsignedCast (^denvarExp1) (Bit32)``
val castExp2 = ``BExp_Cast BIExp_SignedCast (^denvarExp1) (Bit32)``
val castExp3 = ``BExp_Cast BIExp_LowCast (^denvarExp1) (Bit32)``
val castExp4 = ``BExp_Cast BIExp_HighCast (^denvarExp1) (Bit32)``

fun testCastsSimple () = 
		let
			val _ = print "\n"
			val _ = print ((translate_exp castExp1) map)
			val _ = print "\n"
			val _ = print ((translate_exp castExp2) map)
			val _ = print "\n"
			val _ = print ((translate_exp castExp3) map)
			val _ = print "\n"
			val _ = print ((translate_exp castExp4) map)
			val _ = print "\n"
		in
			()
		end


val memory2 = Redblackmap.fromList (String.compare) [("addr", "(_ bv13 64)"), ("mem", "mymemory")]

val store = ``BExp_Store (BExp_Den (BVar "mem" (BType_Mem Bit64 Bit8))) (BExp_Den (BVar "addr" (BType_Imm Bit64))) BEnd_BigEndian (BExp_Const (Imm64 (0x1234w)))``

val loadExp = ``BExp_Load (^store) (BExp_Den (BVar "addr" (BType_Imm Bit64))) BEnd_BigEndian (Bit64)``

fun testStore () =
	print ((translate_exp store) memory2)

fun testLoad () =
	print ((translate_exp loadExp) memory2)

val castComplex = ``BExp_Cast BIExp_SignedCast (^loadExp) Bit32``



(**** Statements *****)


val pred = ``BExp_BinPred BIExp_Equal ^loadExp ^loadExp``
val assertStmt = ``BStmt_Assert ^pred``
val declImm = ``BStmt_Declare (BVar "abc" (BType_Imm Bit64))``
val declMem = ``BStmt_Declare (BVar "efg" (BType_Mem Bit64 Bit8))``
val loadImm = ``BExp_Den (BVar "abc" (BType_Imm Bit64))``
val loadMem = ``BExp_Den (BVar "efg" (BType_Mem Bit64 Bit8))``


val st = init_state "" [] 
fun test_declare_imm() =
	let 
		val decl =translate_stmt declImm
		val lookup = translate_exp loadImm
	in
		print ((lookup (get_env (decl st))) ^ "\n")
	end

fun test_declare_mem() =
	let 
		val decl =translate_stmt declMem
		val lookup = translate_exp loadMem
	in
		print ((lookup (get_env (decl st))) ^ "\n")
	end


(*** statements for controlflow ***)


val jumpImm = ``BStmt_Jmp (BLE_Label (BL_Address (Imm64 0x100w)))``

fun test_jmpImmAddress () = 
	 print ((getPC (hd((translate_cf jumpImm []) st))) ^ "\n")

val jumpExp = ``BStmt_Jmp (BLE_Exp (BExp_Const (Imm64 12w)))``

fun test_jmpExp () =	
	List.map (print o ((C (curry op^)) "\n")  o getPredicate) ((translate_cf jumpExp ["a", "b", "c"]) st)
	


val condJump = ``BStmt_CJmp (BExp_BinPred BIExp_Equal (BExp_Const (Imm64 123w)) (BExp_Const (Imm64 123w))) (BLE_Exp (BExp_Const (Imm64 345w))) (BLE_Exp (BExp_Const (Imm64 987w)))``

fun testCondJump () =
	List.map (print o ((C (curry op^)) "\n")  o getPredicate) ((translate_cf condJump ["a", "b", "c"]) st)



(****** whole programs *******)
val exp = ``(BExp_BinExp BIExp_Plus
    (BExp_Den (BVar "R0" (BType_Imm Bit64)))
    (BExp_Const (Imm64 8w)))``;

val store = ``(BStmt_Assign
	(BVar "MEM" (BType_Mem Bit64  Bit8))
	(BExp_Store (BExp_Den (BVar "MEM" (BType_Mem Bit64 Bit8)))
(BExp_BinExp BIExp_Plus
			   (BExp_Den (BVar "R2" (BType_Imm Bit64)))
			   (BExp_Den (BVar "R3" (BType_Imm Bit64))))
BEnd_LittleEndian
	(BExp_Den (BVar "R2" (BType_Imm Bit64)))))``
val loadlll = ``(BStmt_Assign
	(BVar "R0" (BType_Imm Bit64))
	(BExp_Load (BExp_Den (BVar "MEM" (BType_Mem Bit64 Bit8)))
(BExp_BinExp BIExp_Plus
			   (BExp_Den (BVar "R2" (BType_Imm Bit64)))
			   (BExp_Den (BVar "R3" (BType_Imm Bit64))))
	BEnd_LittleEndian (Bit64)))``
val label = ``BLE_Label (BL_Address (Imm64 5678w))``;
val stmnts = ``[ (^store); (^loadlll)]``
val jump = ``BStmt_CJmp (BExp_BinPred BIExp_Equal (BExp_Const (Imm64 12w)) (BExp_Const (Imm64 5678w))) (^label)  ( ^label)``;
val block = ``<| bb_label := BL_Address (Imm64 5678w); bb_statements := ^stmnts; bb_last_statement := ^jump |>``
val block2 = ``<| bb_label := BL_Address (Imm64 5677w); bb_statements := ^stmnts; bb_last_statement := ^jump |>``
val program = ``BirProgram ([^block;^block2])``

val initalState = init_state "(_ bv5678 64)" [("R0", "R0"), ("R2", "R2"), ("R1", "R2"), ("MEM", "MEM"), ("R3", "R3")]


val caller = ``<| 
	bb_label := BL_Address (Imm64 1337w); 
	bb_statements := []; 
	bb_last_statement := BStmt_Jmp (BLE_Label (BL_Address (Imm64 72w))) |>``

val func = ``<| 
	bb_label := BL_Address (Imm64 72w); 
	bb_last_statement := (BStmt_Jmp (BLE_Exp (BExp_Load (BExp_Den (BVar "MEM" (BType_Mem Bit64 Bit8))) (BExp_Den (BVar "sp" (BType_Imm Bit64))) BEnd_BigEndian (Bit64)))); 
	bb_statements := [(BStmt_Assign 
	(BVar "MEM" (BType_Mem Bit64 Bit8)) 
	(BExp_Store (BExp_Den (BVar "MEM" (BType_Mem Bit64 Bit8))) 
	(BExp_Den (BVar "sp" (BType_Imm Bit64)))
		    BEnd_BigEndian (BExp_Den (BVar "jret" (BType_Imm Bit64)))  ))] |>``
val inlabel = ``BLE_Label (BL_Address (Imm64 1234w))``
val subtract = ``(BStmt_Assign (BVar "R1" (BType_Imm Bit64)) (BExp_BinExp BIExp_Minus (BExp_Den (BVar "R1" (BType_Imm Bit64))) (BExp_Const (Imm64 8w))))``


val readArray = ``(BStmt_Assign (BVar "R0" (BType_Imm Bit64)) (BExp_BinExp BIExp_Plus (BExp_Den (BVar "R0" (BType_Imm Bit64))) (BExp_Load (BExp_Den (BVar "MEM" (BType_Mem Bit64 Bit8))) 
(BExp_BinExp BIExp_Plus 
	(BExp_Den (BVar "R1" (BType_Imm Bit64)))
	(BExp_Den (BVar "R2" (BType_Imm Bit64))))
BEnd_BigEndian (Bit64))))``

val cj_label =
``BStmt_CJmp 
	(BExp_BinPred BIExp_LessOrEqual (BExp_Den (BVar "R2" (BType_Imm Bit64))) (BExp_Const (Imm64 0w)))
	(BLE_Label (BL_Address (Imm64 1234w)))
	(BLE_Label (BL_Address (Imm64 5678w)))``

val outlabel = ``BLE_Label (BL_Address (Imm64 5678w))``



val writeStack = ``BStmt_Assign (BVar "MEM" (BType_Mem Bit64 Bit8)) (BExp_Store (BExp_Den (BVar "MEM" (BType_Mem Bit64 Bit8))) 
(BExp_BinExp BIExp_Plus (BExp_Den (BVar "SP" (BType_Imm Bit64)))
	(BExp_Den (BVar "ucd" (BType_Imm Bit64))))
BEnd_BigEndian (BExp_Den (BVar "mip" (BType_Imm Bit64))))``
val jl = ``BStmt_Jmp (BLE_Exp ( BExp_Load (BExp_Den (BVar "MEM" (BType_Mem Bit64 Bit8))) 
(BExp_BinExp BIExp_Plus (BExp_Den (BVar "SP" (BType_Imm Bit64))) (BExp_Const (Imm64 32w))) BEnd_BigEndian (Bit64)))``


val outlabel1 = `` (BL_Address (Imm64 5678w))``
val decl1 = ``BStmt_Declare (BVar "Xold" (BType_Imm Bit64))``
val decl2 = ``BStmt_Declare (BVar "Yold" (BType_Imm Bit64))``
val decl3 = ``BStmt_Declare (BVar "X" (BType_Imm Bit64))``
val decl4 = ``BStmt_Declare (BVar "Y" (BType_Imm Bit64))``
val sw1 = ``BStmt_Assign (BVar "Xold" (BType_Imm Bit64)) (BExp_Den (BVar "X" (BType_Imm Bit64))) ``
val sw2 = ``BStmt_Assign (BVar "Yold" (BType_Imm Bit64)) (BExp_Den (BVar "Y" (BType_Imm Bit64))) ``
val sw3 = ``BStmt_Assign (BVar "X" (BType_Imm Bit64)) (BExp_BinExp BIExp_Plus (BExp_Den (BVar "X" (BType_Imm Bit64))) (BExp_Den (BVar "Y" (BType_Imm Bit64))))``
val sw7 = ``BStmt_Assign (BVar "Y" (BType_Imm Bit64)) (BExp_BinExp BIExp_Minus (BExp_Den (BVar "X" (BType_Imm Bit64))) (BExp_Den (BVar "Y" (BType_Imm Bit64))))``
val sw12 = ``BStmt_Assign (BVar "X" (BType_Imm Bit64)) (BExp_BinExp BIExp_Minus (BExp_Den (BVar "X" (BType_Imm Bit64))) (BExp_Den (BVar "Y" (BType_Imm Bit64))))``
val ass111 = ``BStmt_Assert (BExp_BinPred BIExp_Equal (BExp_Den (BVar "Xold" (BType_Imm Bit64))) (BExp_Den (BVar "Y" (BType_Imm Bit64))))``
val ass1 = ``BStmt_Assert (BExp_BinPred BIExp_Equal (BExp_Den (BVar "Yold" (BType_Imm Bit64))) (BExp_Den (BVar "X" (BType_Imm Bit64))))``
val h = ``BStmt_Halt (BExp_Const (Imm64 (0w)))``

val blk = ``<| bb_label := ^outlabel1; bb_statements := [^decl1;^decl2; ^decl3; ^decl4;^sw1;^sw2;^sw3;^sw7;^sw12;^ass111;^ass1]; bb_last_statement := ^h |>``
val prg = ``BirProgram [^blk]``

val e = transl prg 
val initalState = init_state "(_ bv5678 64)" [("X", "X"), ("Y", "Y"), ("Xold", "Xold"), ("Yold", "Yold")]




(*
 fun test_translate() =
 let
 val prg = transl progam
 *)	


end
