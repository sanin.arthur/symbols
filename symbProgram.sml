

open HolKernel 
open Parse
open Redblackmap
open bir_programTheory 
open bir_typing_progTheory
open symbJumps
open symbStmt
open symbExp
open symbState
open OS.Process OS.FileSys
open listSyntax
open ListPair
open pred_setSyntax

structure symbProgram = struct

fun makeProgramList program =
	let
		val blocks = (fst o dest_list o dest_BirProgram) program
		val records = List.map dest_bir_block_list blocks
		val interim = List.map (fn (a,b,c,d) => (b, (c,d))) records
	in
		interim
	end


fun getVarsInBlock (stBlist, stE) =
	let
		val varB = List.map (fn st => (rhs o concl o EVAL) ``bir_vars_of_stmtB ^st``) stBlist
		val varE = (rhs o concl o EVAL) ``bir_vars_of_stmtE ^stE``
	in
		(rhs o concl o EVAL) (List.foldl ( mk_union) varE varB)
	end


fun getVarsInProgram prgmap =
	(rhs o concl o EVAL) (List.foldl mk_union empty_tm (List.map getVarsInBlock prgmap))




(* assume label are made of imm64 *)
fun translate_block  allLabels (d,f) =
	 (List.map translate_stmt d, translate_cf f allLabels)

fun compose_block  (d,f) =  f o (List.foldl (fn (func, comp) => func o comp) I d)

fun compose_map label_block_list = 
	Redblackmap.fromList (String.compare) 
	(List.map (fn (label, block) => (label,  compose_block block)) label_block_list)  


(* (a,(b,c)) *)
fun transl program =
	let
		val label_stmt_list = makeProgramList program
		val allLabels = List.map (fn (a,b) => (imm64_to_word o dest_BL_Address) a) label_stmt_list
		val label_blocks_list = List.map (fn (label, block) => ((imm64_to_word o dest_BL_Address) label, translate_block allLabels (block))) label_stmt_list
	in
		compose_map label_blocks_list
	end



datatype 'a tree = T of 'a * 'a tree list
fun make_leaf n = T(n,[])


fun execute map state =
	let
		val block = (Redblackmap.peek (map, (getPC state)))
		val exec = case block of 
				SOME b => b state 
				| NONE => [change_status state (ERROR "PC invalid")]
	in
		case  exec of 
			(e::nil) => if get_state(e) = RUNNING andalso not (getPC e = getPC state)
					then execute  map e
					else [e]
			| l => l 
	end
	



datatype isSat = SAT | UNSAT | UNKNOWN
exception LEX of string
fun lex [] = []
  | lex ((#"\n")::b) = lex b
  | lex ((#"s")::(#"a")::(#"t")::b) = SAT::lex b
  | lex ((#"u")::(#"n")::(#"s")::(#"a")::(#"t")::b) = UNSAT :: lex b
  | lex ((#"u")::(#"n")::(#"k")::(#"n")::(#"o")::(#"w")::(#"n")::b) = UNKNOWN :: lex b
  | lex g = raise LEX (implode g)




fun process (SAT) state =
	let
		val tmp = OS.FileSys.tmpName()
		val command = "(cat " ^ (getWritef state) ^ " > " ^ tmp ^ ") && (echo \"" ^ (getPredicate state) ^ "\" >> " ^ tmp ^ ")"
		val _ = OS.Process.system command
	in
		change_writef state tmp
	end
  | process (UNSAT) state = change_status state UNSATISFIABLE
  | process (UNKNOWN) state = change_status state (ERROR "PRED TOO HARD")

fun isolate_cond state = "(push 1)" ^ (getPredicate state) ^ "(check-sat)(pop 1)"


fun prep_check_sat oldstate newstates = 
	let
		val tmp = OS.FileSys.tmpName ()
		val dp = (List.foldl (fn (a,b) => b ^ (isolate_cond a)) "" newstates) 
		val command = "(cat " ^ (getWritef oldstate) ^ " > " ^ tmp ^ ") && (echo \"" ^ dp ^ "\" >> "  ^ tmp ^ ") && (stp " ^ tmp ^ " > " ^ (getReadf oldstate) ^ " ) " 
	in
		(tmp,command)
	end

fun check_sat oldstate newstates =
	let
		val (file,cmd) = prep_check_sat oldstate newstates
		val _ = OS.Process.system cmd
		val resultStream = TextIO.openIn (getReadf oldstate)
		val result = TextIO.inputAll resultStream
		val close = TextIO.closeIn resultStream
	in
		lex (explode result)
	end


fun processing map state =
	let
		val newstates = execute map (state)
		val sats = check_sat state newstates
	in
		ListPair.mapEq (make_leaf o (uncurry process)) (sats,newstates)
	end
	
fun execute_tree map (T(a, [])) =
	 if get_state a = RUNNING  
	 then  let val exec = (processing map (set_predicate a ""))
	       in  T(set_predicate a "",  exec) end
	 else T(a,[])
   |execute_tree map (T(a,b)) = T(a, List.map (execute_tree map) b)


end
