


open HolKernel Parse
open stringSyntax
open wordsTheory
open bir_programTheory
open bir_programSyntax
open bir_immSyntax
open Redblackmap
open String
open Lib
open symbExp
open symbState
open symbStmt

structure symbJumps = struct
datatype jmptokken = HALT | JMPIMM | JMPEXP | JMPSTRING | CJMP

val jmpMapping = [
(``BStmt_Halt e``, HALT),
(``BStmt_Jmp (BLE_Exp a)``, JMPEXP),
(``BStmt_Jmp (BLE_Label (BL_Label a ))``, JMPSTRING),
(``BStmt_Jmp (BLE_Label (BL_Address a))``, JMPIMM),
(``BStmt_CJmp a b c ``, CJMP)] 

val jmpNet = List.foldl (fn (m,net) => Net.insert m net) Net.empty jmpMapping

fun imm64_to_word imm = 
		let 
		    val vec = (term_to_string o (rhs o concl o EVAL)) ``w2n ^(dest_Imm64 imm)``
		    val prefix = "(_ bv"
		    val postfix = " 64)"
		in prefix ^ vec ^ postfix
		end
(* ignore string labels... *)
fun translate_label label = 
	if is_BLE_Label label 
		then  (fn env =>(imm64_to_word o dest_BL_Address o dest_BLE_Label) label)
		else (translate_exp o dest_BLE_Exp) label
			
exception JUMPMATCH of term
fun translate_cf stmt program = 
	case Net.match stmt jmpNet of
		  [HALT] => translate_halt stmt program
		| [JMPIMM] => translate_jump_label_imm stmt program 
		| [JMPEXP] => translate_jump_to_exp stmt program
		| [JMPSTRING] => translate_jump_label_string stmt program 
		| [CJMP] => translate_cond_jump stmt program 
		| a => raise JUMPMATCH stmt

and translate_halt stmt program =
	let
		val exp = translate_exp (dest_BStmt_Halt stmt)
	in
		fn (state:state) =>  ([change_status state (HALTED (exp (#env state)))]:state list)
	end



(*assume addresses of size 64 for now *)
and translate_jump_label_imm stmt program =
	let
		val address = (imm64_to_word o dest_BL_Address o dest_BLE_Label o dest_BStmt_Jmp) stmt
	in 
		(fn (state:state) => [change_pc state address]:state list)
	end
	

and translate_jump_label_string stmt program =
	let
		val label = (fromHOLstring o dest_BL_Label o dest_BLE_Label o dest_BStmt_Jmp) stmt
	in
		 (fn (state:state) => [change_pc state label]:state list)
	end


and translate_jump_to_exp stmt program =
	let
		val exp = translate_exp ((dest_BLE_Exp o dest_BStmt_Jmp) stmt)
	in
		(fn (state:state) => List.map (fn addr => 
	update_predicate (change_pc state addr) ("(assert (= " ^ (exp (#env state)) ^"(" ^ addr ^ ")))"))  program)
	end



and translate_cond_jump stmt program =
	let
		val (cond, tr, fa) = dest_BStmt_CJmp stmt
		val textedCond = translate_exp cond
		val trueCase = translate_label tr
		val falseCase = translate_label fa
	in
		fn (state:state) =>  let 
			val {env,...} = state
			in	
			List.map (fn addr => update_predicate (change_pc state addr) ("(assert (= " ^ addr ^ " (ite (= #b1 " ^ (textedCond env) ^ ") " ^ (trueCase env) ^ " " ^ (falseCase env) ^ ")))")) program
			end
	end

end
