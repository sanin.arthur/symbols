

open HolKernel Parse Term
open Net
open stringSyntax
open bir_programTheory
open bir_programSyntax
open bir_valuesSyntax
open stringTheory 
open stringSyntax
open symbExp
open symbState


structure symbStmt = struct

datatype stmtToken = ASSIGN | ASSERT | ASSUME | OBSERVE | DECLARE
(*
val stmtMapping = [(``BStmt_Assign a b``, ASSIGN),
		   (``BStmt_Assert a``, ASSERT),
		   (``BStmt_Assume a``, ASSUME),
		   (``BStmt_Declare a``, DECLARE),
		   (``BStmt_Observe a b c``, OBSERVE)]
*)


fun translate_assert stmt = 
	let
		val exp = translate_exp (dest_BStmt_Assert stmt)
	in
		fn (state:state) =>update_predicate state  ("( assert (= #b1 " ^ (exp (#env state)) ^ " ))")
	end


fun translate_assume stmt =
	let
		val exp = translate_exp (dest_BStmt_Assume stmt)
	in
		fn (state:state) =>  update_predicate state ("( assert (= #b1 " ^ (exp (#env state)) ^ " ))")
	end


fun translate_assign stmt = 
	let
		val (var, exp) = (dest_BStmt_Assign stmt)
		val (id, ty) = dest_BVar var
		val value = translate_exp exp 
	in
		fn state => assign_variable state (fromHOLstring id) (value (#env state)) 
	end
		
(*TODO probalbly antoher destroy for the variable is needed *)
val ts = fn ty => (term_to_string o rhs o concl o EVAL) ``size_of_bir_immtype ^( ty)``

fun initValue var a (NONE) = "(declare-fun " ^ (fromHOLstring var) ^ " () (_ BitVec " ^ (ts (dest_BType_Imm a)) ^ "))"
   |initValue var a (SOME b) = "(declare-fun " ^ (fromHOLstring var) ^ " () (Array (_ BitVec " ^ (ts a) ^ ") (_ BitVec " ^ (ts b) ^ ")))"

fun sndSome (a,b) = (a, SOME b)
fun translate_declare stmt =
	let
		val (var, ty) = (dest_BVar o dest_BStmt_Declare) stmt
		val iv = if is_BType_Imm ty 
			    then initValue var ty  NONE
			    else (uncurry (initValue var)) (sndSome (dest_BType_Mem ty))
	in
	fn state =>(update_predicate  (declare_variable state (fromHOLstring var, fromHOLstring var)) iv)
	end


val stmtMapping = [(``BStmt_Assign a b``, translate_assign),
		   (``BStmt_Assert a``, translate_assert),
		   (``BStmt_Assume a``, translate_assume),
		   (``BStmt_Declare a``, translate_declare)
		   (*(``BStmt_Observe a b c``, translate_observe) *)
]

val stmtNet = List.foldl (fn (m,net) => Net.insert m net) Net.empty stmtMapping

exception STMTMATCHERROR of term
fun translate_stmt stmt =
	case Net.match stmt stmtNet of
	(t::nil) => t stmt 
	| a => raise STMTMATCHERROR stmt
end

