

load "symbExp"
load "symbState"
load "symbStmt"
load "symbJumps"
load "symbProgram"
load "unitTest"
load "init"
open symbState
open symbExp
open symbStmt
open symbJumps
open symbProgram
open init
open unitTest
open OS.Process
(*
system ("echo \"" ^ logic ^ "\n" ^ rr ^ "\" | stp" )
memBEfunc
val rr = "(declare-fun abc () (Array (_ BitVec 64) (_ BitVec 8)))"
val tt = "(declare-fun x () (_ BitVec 64))"
val assert1 = "(assert (= x (_ bv1234567 64)))"
val store1 = mkStoreBE "abc"  "(_ bv0 64)" "x"
val load1 = mkLoadBE store1 "(_ bv0 64)"
val assert2 = "(assert (not (= " ^load1^ " (_ bv1234567 64))))"
val sat =  rr ^  tt ^ assert1 ^ assert2 ^ "(check-sat)(get-value ((x)))"
system ("echo \"" ^ sat ^ "\" | stp" )
mkLoadBE "a" "b"
mkStoreBE "a" "b" "c"

val pr = transl program
val f = execute_tree pr
f(f )
val a =(execute_tree pr (T(initalState,[])))
process SAT a

val b =(execute_tree prg (T(initalState, [])))

listItems prg
val t =(snd o hd) (Redblackmap.listItems prg)
t initalState
listItems (get_env initalState)
testCondJump()
 (C (curry op^)) "\n"
test_jmpExp()
test_jmpImmAddress()
test_declare_mem ()
val t =get_env (decl st)
listItems t
val r = lookup
r t
curry op^
thm_to_string
type_of_bir_exp_def
EVAL ``THE (SOME a)``
test_unary()
testCastsSimple()
test_const_exp ()
test_binExp()
(translate_exp store) rr
val rr =Redblackmap.insertList (map,[("addr", "(_ bv12 64)"), ("mem", "mymem")])
translate_exp castComplex
castComplex
testStore()
testLoad()
translate_exp castComplex
*)

