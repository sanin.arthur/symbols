
open HolKernel Parse
open bir_typing_progTheory
open Redblackmap
open String
open bir_programSyntax
open OS.FileSys


structure symbState =
struct

datatype status = RUNNING | ERROR of string | HALTED of string | UNSATISFIABLE
type state = {pc : string, st : status, env : (string, string) dict , predicate : string, readf : string, writef: string  }




fun init_state pc vars = { pc=pc , st = RUNNING, env = Redblackmap.fromList (String.compare) vars, predicate = "", writef=OS.FileSys.tmpName (), readf = OS.FileSys.tmpName()}:state 


fun update_predicate ({readf, writef,pc,st,env,predicate}:state) new_predicate =
	{readf=readf, writef=writef, pc=pc, st=st, env=env, predicate=predicate ^ new_predicate}:state

fun set_predicate ({readf,writef,pc,st,env,predicate}:state) new_predicate =
	{readf=readf, writef=writef, pc=pc, st=st, env=env, predicate= new_predicate}:state

fun change_pc ({readf, writef, pc, st, env, predicate}: state) new_pc =
	 {readf=readf, writef=writef,pc=new_pc, st=st, env=env, predicate=predicate}:state 

fun change_status ({readf, writef,pc, st, env, predicate}:state) new_status = 
	{readf=readf, writef=writef,pc=pc, st=new_status, env=env, predicate=predicate}:state

fun change_writef ({readf, writef,pc, st, env, predicate}:state) filename = 
	{readf=readf, writef=filename,pc=pc, st=st, env=env, predicate=predicate}:state


fun get_env ({env,...}:state) = env
fun get_state ({st,...}:state) = st
fun getPC ({pc,...}:state) = pc
fun getWritef ({writef,...}:state) = writef
fun getReadf ({readf,...}:state) = readf
fun getPredicate ({predicate,...}:state) = predicate

fun declare_variable ({readf,writef,pc, st, env, predicate}:state) (var, initValue) =
	case Redblackmap.peek (env, var) of
		NONE => {readf=readf, writef=writef,pc=pc, st=st, env= Redblackmap.insert (env, var,initValue), predicate=predicate}:state
		| SOME _ => change_status ({readf=readf, writef=writef,pc=pc, st=st, env=env, predicate=predicate}) (ERROR "Var already Declared")

fun assign_variable ({readf, writef,pc,st,env, predicate}:state) var value =
	case Redblackmap.peek (env, var) of
		NONE => change_status ({readf=readf, writef=writef,pc=pc, st=st, env=env, predicate=predicate}) (ERROR ("Tried to assign to non-existent variable:" ^ (var)))
		| SOME _ => {readf=readf, writef=writef,pc=pc, st=st, env= Redblackmap.insert (env, var,  value), predicate=predicate}:state

end
