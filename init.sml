
structure init = 
struct
val ext = fn (value, ende, start) => "((_ extract " ^ ende ^ " " ^ start ^ ") " ^  value ^ ")"

val add = fn (address, offset) => "(bvadd " ^  address ^ " (_ bv" ^ offset ^ " 64))"

val st = fn (mem, addr, value) =>  "(store " ^ mem ^ " " ^ addr ^ " " ^ value ^ ")"


fun makeStoreBE 0 ende start offset memory address value =  memory
  | makeStoreBE count ende start  offset memory address value = 
		makeStoreBE (count-1) (ende + 8) (start + 8) (offset + 1) (st (memory, add (address, (Int.toString offset)), ext(value, Int.toString ende, Int.toString start))) address value

fun mkStoreBE memory address value = makeStoreBE 8 7 0 0 memory address value


fun concatFromMemory current address memory = "(concat (select " ^ memory ^ " " ^ address ^ ")"  ^ current ^ ")"


fun loadBE 0     offset memory address current = current
  | loadBE count offset memory address current = loadBE (count-1) (offset +1) memory address (concatFromMemory current (add (address, Int.toString offset)) memory)


fun mkLoadBE  memory address = loadBE 7 1 memory address ("(select " ^ memory ^ " " ^ (add (address, "0")) ^ ")")



end
