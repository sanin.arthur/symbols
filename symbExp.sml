

open HolKernel Parse Term boolLib bossLib
open boolTheory 
open liteLib
open boolSyntax
open wordsTheory
open wordsSyntax
open Arbnumcore
open stringSyntax
open HolBACoreSimps
open bir_valuesSyntax
open bir_expSyntax
open bir_expTheory
open bir_mem_expSyntax
open bir_mem_expTheory
open bir_imm_expSyntax
open bir_immSyntax
open bir_envSyntax
open bir_envTheory
open bir_expTheory
open bir_expSyntax
open bir_valuesTheory
open bir_typing_expTheory
open Redblackmap
open stringSyntax
open Net
open init

structure symbExp =
struct
(*Assume coorect typing for now *)
fun strComp (a,b) = Int.compare (the (Int.fromString a), the (Int.fromString b))

fun strSub (a,b) = Int.toString (Int.- (the (Int.fromString a),(the (Int.fromString b))))

fun bir_scast EQUAL exp oldty newty = (fn env => (exp env))
   |bir_scast LESS exp oldty newty =  (fn env => "((_ sign_extend " ^ (strSub(newty, oldty)) ^ ") " ^ (exp env) ^ ")")
   |bir_scast GREATER exp oldty newty = (fn env => "((_ extract " ^ (strSub (newty, "1")) ^ " 0) " ^ (exp env) ^ ")")

fun bir_cast EQUAL exp oldty newty = (fn env => (exp env))
  | bir_cast LESS exp oldty newty = (fn env => "((_ concat (_ bv" ^ (strSub(newty,oldty)) ^ ") " ^ (exp env) ^ ")")
  | bir_cast GREATER exp oldty newty = (fn env => "((_ extract " ^ (strSub(newty,"1")) ^ " 0) " ^ (exp env) ^ ")")

fun bir_lcast a b c d= bir_cast a b c d

fun bir_hcast EQUAL exp old new =( fn env => (exp env))
  | bir_hcast LESS exp old new = (bir_cast LESS exp old new)
  | bir_hcast GREATER exp old new = (fn env => "((_ extract " ^ (strSub(old, "1")) ^ " " ^ new ^ ") " ^ (exp env) ^ ")")


val castMapping = [
	(``BIExp_UnsignedCast``, bir_cast),
	(``BIExp_SignedCast``, bir_scast),
	(``BIExp_HighCast``, bir_hcast), 
	(``BIExp_LowCast``, bir_lcast)]
val castNet = List.foldl (fn (m,net) => Net.insert m net) Net.empty castMapping


datatype expTokens = CONST1 |CONST8 |CONST16 |CONST32 |CONST64 | DEN | CAST | UNARY | BIN | MEMEQ | ITE | LOAD | STORE | PRED


val expMapping = [
(``BExp_Const (Imm1 a)``, CONST1),
(``BExp_Const (Imm8 a)``, CONST8),
(``BExp_Const (Imm16 a)``, CONST16),
(``BExp_Const (Imm32 a)``, CONST32),
(``BExp_Const (Imm64 a)``, CONST64),
(``BExp_Den a``, DEN),
(``BExp_Cast a b c``, CAST),
(``BExp_UnaryExp a b``, UNARY),
(``BExp_BinExp a b c``, BIN),
(``BExp_MemEq a b``, MEMEQ),
(``BExp_IfThenElse a b c``, ITE),
(``BExp_Load a b c d``, LOAD),
(``BExp_Store a b c d``, STORE),
(``BExp_BinPred a b c``, PRED)]

val expNet = List.foldl (fn (m,net) => Net.insert m net) Net.empty expMapping


val predMapping = [
(``BIExp_Equal``, "="),
(``BIExp_NotEqual``, "distinct"),
(``BIExp_LessThan``, "bvult"),
(``BIExp_SignedLessThan``, "bvslt"),
(``BIExp_LessOrEqual``, "bvule"),
(``BIExp_SignedLessOrEqual``, "bvsle")]

val predNet = List.foldl (fn (m,net) => Net.insert m net) Net.empty predMapping

(* we do not implement counting of leading bits ... *)
val unaryMapping = [
(``BIExp_ChangeSign``, "bvneg"),
(``BIExp_Not``, "bvnot")]

val unaryNet = List.foldl (fn (m,net) => Net.insert m net) Net.empty unaryMapping



val binMapping =[
(``BIExp_And``, "bvand"),
(``BIExp_Or``, "bvor"),
(``BIExp_Xor``, "bvxor"),
(``BIExp_Plus``, "bvadd"),
(``BIExp_Minus``, "bvsub"),
(``BIExp_Mult``, "bvmul"),
(``BIExp_Div``, "bvudiv"),
(``BIExp_SignedDiv``, "bvsdiv"),
(``BIExp_Mod``, "bvurem"),
(``BIExp_SignedMod``, "bvsrem"),
(``BIExp_LeftShift``, "bvlshl"),
(``BIExp_RightShift``, "bvlshr"),
(``BIExp_SignedRightShift``, "bvashr") ]

val binNet = List.foldl (fn (m,net) => Net.insert m net) Net.empty binMapping

fun translateConst (typedest,sz) exp =
	let 
		val bv = ((term_to_string o (rhs o concl o EVAL)) ``w2n ^(typedest exp)``)
		val prefix = "(_ bv" 
		val postfix =  " " ^ (Int.toString sz) ^ ")"
	in
		fn env => prefix ^ bv ^ postfix
	end
(* handle Error of the later *)
fun translate_den name  = (fn env =>
	let 
		val t = (Redblackmap.peek (env,name))
	in 
		the t
	end)

exception NotImplementedYet of term 
(* TODO still not caring about typing *)
fun translate_exp exp = 
	case Net.match exp expNet  of
		 [CONST1] => translateConst (dest_Imm1 o dest_BExp_Const,1) exp
		|[CONST8] => translateConst (dest_Imm8 o dest_BExp_Const,2) exp
		|[CONST16] => translateConst  (dest_Imm16 o dest_BExp_Const,4) exp
		|[CONST32] => translateConst  (dest_Imm32 o dest_BExp_Const,8) exp
		|[CONST64] => translateConst  (dest_Imm64 o dest_BExp_Const,16) exp
		|[DEN] => translate_den (fromHOLstring (#1(( dest_BVar o dest_BExp_Den) exp)))
		|[CAST] => translate_cast exp
		|[UNARY] => translate_unary exp 
		|[BIN] => translate_bin exp 
		|[PRED] => translate_pred exp
		|[ITE] => translate_ite exp
		|[LOAD] => translate_load exp 
		|[STORE] => translate_store exp
		| a  => raise NotImplementedYet exp

and translate_store exp  =
	let 
		val (mem, addr, endian, value) =  dest_BExp_Store exp
		val textMem = translate_exp mem 
		val textAddr = translate_exp addr 
		val textValue = translate_exp value
	in
		fn env =>  mkStoreBE (textMem env )  (textAddr env) (textValue env) 
	end

and translate_load exp = 
	let 
		val (mem, addr, endian, ty) =  dest_BExp_Load exp
		val textMem = translate_exp mem 
		val textAddr = translate_exp addr 
	in 
		fn env => mkLoadBE (textMem env) (textAddr env) 
	end 

		



and translate_pred exp = 
	let
		val (operator, left, right) = dest_BExp_BinPred exp
		val lefttext = translate_exp left
		val righttext = translate_exp right
		val textedOperator =hd ( Net.match operator predNet)
	in
		fn env => "(ite (" ^ textedOperator ^ " " ^ (lefttext env) ^ " " ^ (righttext env) ^ ") (#b1) (#b0))" 	
	end




and translate_ite exp =
	let
		val (i,t,e) = dest_BExp_IfThenElse exp
		val sIF = translate_exp exp 
		val sTHEN = translate_exp exp 
		val sELSE = translate_exp exp 
	in
		fn env => " ( ite " ^ (sIF env) ^ (sTHEN env) ^ (sELSE env) ^ " ) "
	end

and translate_bin exp =
	let
		val (binop, lexp, rexp) = dest_BExp_BinExp exp
		val textedlexp = translate_exp lexp 
		val textedrexp = translate_exp  rexp 
		val textedop = hd (Net.match binop binNet)
	in
		fn env => " ( " ^ textedop ^ " " ^ (textedlexp env) ^  " " ^ (textedrexp env) ^ " ) " 
	end

and translate_cast exp =
	let
		val (cast, innerexp, newty) = dest_BExp_Cast exp
		val exp = translate_exp innerexp
		val old = SIMP_CONV (srw_ss()++holBACore_ss) 
			[bir_var_type_def,
			 type_of_bir_exp_def,
			 bir_type_is_Imm_def,
			 bir_number_of_mem_splits_REWRS,
			 bir_type_is_Mem_def] ``THE (type_of_bir_exp ^innerexp)``	
		val oldtype = (term_to_string o rhs o concl o EVAL) ``size_of_bir_immtype ^((dest_BType_Imm o rhs o concl)old)``
		val newtype = ( term_to_string o rhs o concl o EVAL) ``size_of_bir_immtype ^newty``
		val cmp = strComp(oldtype, newtype)
		val func = hd (Net.match cast castNet)
	in
		fn env => func (cmp) exp oldtype newtype env
	end	

(* counting leading bits and counting singned leading bits is not implemented *)
and translate_unary exp = 
	let
		val (operator, innerexp) = dest_BExp_UnaryExp exp
		val innertext = translate_exp innerexp 
		val textedOperator = hd (Net.match operator unaryNet)
	in
		fn env => "(" ^ textedOperator ^ " " ^ (innertext env) ^ ")" 	
	end
end
